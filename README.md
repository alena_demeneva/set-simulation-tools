This is the README file for launching automated tools for single-event transient (SET) simulation

The developed programs have been tested on these operating systems:  
Linux GNOME 2.28.2, python version 2.6.6 for SPICE_simulation_for_schemes.py and SPICE_simulation_for_cells.py  
Windows 7+, python 3.7 for transistor_logic_model.py

Input data:  
Cells - spice netlists of the cells (used for characterization)  
SPCircuits - spice netlists of the circuits  
NangateOpenCellLibrary_PDKv1_3_v2009_07 - library  
param_file - parameter file  
technology - models of the transistors

Running programs SPICE simulation:  
Via Linux terminal:  
python SPICE_simulation_for_circuits.py SPCircuits param_file number_of_simulations  
python SPICE_simulation_for_cells.py Cells param_file

It's important!  
The folder with the circuits (cells), the parameter file and the executable script must be in the same directory  
You must specify the path to the 'technology' folder and experimental parameters in the 'param_file'. Unzip the 'technology' folder beforehand  
After cell characterization, failure probabilities are arranged as in the truth table

Running program Transistor logic simulation:  
Via cmd terminal:  
python transistor_logic_model.py SPCircuits output_file

Logic simulation using pre-characterized probabilities of failure for standard cells is situated in another project