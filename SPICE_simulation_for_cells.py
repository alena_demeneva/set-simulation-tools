#!/usr/bin/python

import sys
import time
import os
import re
import subprocess
import shutil
import random

start_time = time.time()

"""
H - 1
L - 0
X - two open paths
Z - no open path
"""

_pmos_table = {('H', 'H'): 'Z',
               ('H', 'Z'): 'Z',
               ('L', 'H'): 'H',
               ('L', 'Z'): 'Z',
               ('X', 'H'): 'H',
               ('X', 'Z'): 'Z',
               ('Z', 'H'): 'Z',
               ('Z', 'L'): 'Z',
               ('Z', 'Z'): 'Z'}

_nmos_table = {('H', 'L'): 'L',
               ('H', 'Z'): 'Z',
               ('L', 'L'): 'Z',
               ('L', 'Z'): 'Z',
               ('X', 'L'): 'L',
               ('X', 'Z'): 'Z',
               ('X', 'Z'): 'Z',
               ('Z', 'H'): 'Z',
               ('Z', 'L'): 'Z',
               ('Z', 'Z'): 'Z'}


def truth_table(n):
    if n < 1:
        return [[]]
    t = truth_table(n - 1)
    return [row + [v] for row in t for v in [0, 1]]


class Transistor:
    def __init__(self, label, drain, gate, source, bulk, model):
        self.label = label
        self.pins = {}
        drain.connect(self, 'drain')
        gate.connect(self, 'gate')
        source.connect(self, 'source')
        bulk.connect(self, 'bulk')
        self.model = model

    def propagate(self, gate_signal, source_signal):
        if self.model == 'PMOS_VTL':
            return _pmos_table[(gate_signal, source_signal)]
        return _nmos_table[(gate_signal, source_signal)]


class Net:
    def __init__(self, label):
        self.label = label
        self.connections = set()

    def connect(self, transistor, pin):
        self.connections.add((transistor, pin))
        transistor.pins[pin] = self

    def disconnect(self, transistor, pin):
        self.connections.remove((transistor, pin))
        transistor.pins[pin] = None


class Circuit:
    def __init__(self, name=None):
        self.name = name
        self.inputs = []
        self.outputs = []
        self.transistors = {}
        self.nets = {}

    @classmethod
    def from_sp(cls, file):
        circuit = cls()

        with open(file, 'r') as f:
            for line in f:
                line = line[:-1].strip()

                if line.startswith('*'):
                    pair = line.strip('*').split(':')
                    if len(pair) != 2:
                        continue
                    param, val = pair
                    param = param.strip()
                    val = val.strip()
                    if param.lower() == 'inputs':
                        circuit.inputs.extend(val.split())
                    elif param.lower() == 'outputs':
                        circuit.outputs.extend(val.split())

                elif line.startswith('.SUBCKT'):
                    _, circuit.name, _ = line.split(' ', 2)

                elif line.startswith('M'):
                    label, d, g, s, b, model, _ = line.split(' ', 6)
                    for net_label in (d, g, s, b):
                        if net_label not in circuit.nets:
                            circuit.nets[net_label] = Net(net_label)
                    circuit.transistors[label] = Transistor(label, circuit.nets[d], circuit.nets[g], circuit.nets[s],
                                                            circuit.nets[b], model)

        return circuit

    def simulate(self, inputs):
        """
        :param inputs: {<net name>: <signal value>, ...}
        :return: {<net name>: <signal value>, ...}
        """
        for net_name, val in inputs.items():
            if val == 1:
                inputs[net_name] = 'H'
            elif val == 0:
                inputs[net_name] = 'L'

        signals = {}
        for net_name, val in inputs.items():
            signals[self.nets[net_name]] = val

        signals[self.nets['VDD']] = 'H'
        signals[self.nets['VSS']] = 'L'

        temp_signals = {}
        for net in self.nets.values():
            if net not in signals:
                temp_signals[net] = set()

        net_unknown_inputs = {}
        tr_unknown_inputs = {}

        for net in self.nets.values():
            if net in signals:
                continue
            inps = 0
            for tr, pin in net.connections:
                if pin == 'drain':
                    inps += 1
            net_unknown_inputs[net] = inps

        for tr in self.transistors.values():
            inps = 0
            if tr.pins['gate'] not in signals:
                inps += 1
            if tr.pins['source'] not in signals:
                inps += 1
            tr_unknown_inputs[tr] = inps

        while tr_unknown_inputs:
            tr_ready = []
            net_ready = []
            for tr, inps in tr_unknown_inputs.items():
                if inps == 0:
                    tr_ready.append(tr)

            for tr in tr_ready:
                del tr_unknown_inputs[tr]
                gate_net = tr.pins['gate']
                gate_signal = signals[gate_net]
                source_net = tr.pins['source']
                source_signal = signals[source_net]
                drain_net = tr.pins['drain']
                drain_signal = tr.propagate(gate_signal, source_signal)
                temp_signals[drain_net].add(drain_signal)

                net_unknown_inputs[drain_net] -= 1

            for net, inps in net_unknown_inputs.items():
                if inps == 0:
                    net_ready.append(net)

            for net in net_ready:
                del net_unknown_inputs[net]
                temp = temp_signals[net]

                if 'H' in temp and 'L' in temp:
                    signals[net] = 'X'
                elif 'H' in temp:
                    signals[net] = 'H'
                elif 'L' in temp:
                    signals[net] = 'L'
                else:
                    signals[net] = 'Z'

                for tr, pin in net.connections:
                    if pin in ['source', 'gate']:
                        tr_unknown_inputs[tr] -= 1

        modes = {}
        for tr in self.transistors.values():
            if signals[tr.pins['gate']] == 'L' and tr.model == 'PMOS_VTL':
                modes[tr.label] = 0
            elif signals[tr.pins['gate']] == 'H' and tr.model == 'NMOS_VTL':
                modes[tr.label] = 0
            else:
                modes[tr.label] = 1

        out_signals = {}

        for out_name in self.outputs:
            out_signals[out_name] = signals[self.nets[out_name]]

        return modes, out_signals


ITER = 0
ERRORS_ST_ONE = 0
ERRORS_ST_ZERO = 0


start_time = time.time()


def repl2(y):
    x = str(y.group(1))
    x += '1'
    return "{0}".format(x)


def parameters_of_transistors_parser(hspice_file):
    list1 = list()
    list2 = list()
    with open(hspice_file, 'r') as fread:
        for line in fread:
            m = re.search(r"^(M_i_\w+)\s+\w+\s\w+\s\w+\s\w+\s+\w+\s+([W].+)", line)
            if m:
                list1.append(m.group(1))
                line_new2 = re.sub(r"(W|L|AS|AD|PS|PD)", repl2, m.group(2))
                list2.append(line_new2)
    d = dict(zip(list1, list2))
    return d

def hspice_parser(hspice_file, param_file):
    circuit = Circuit.from_sp(hspice_file)
    fwrite = open('new_hspice_file.sp', 'w')
    fwrite.write('* new_scheme \n')

    with open(param_file, 'r') as fread:
        for line in fread:
            m1 = re.search(r"File path model:\s(.*)", line)
            if m1:
                fwrite.write('.option search=`' + m1.group(1) + '`\n')
            m2 = re.search(r"Capacitance, in pico farads:\s(.*)", line)
            if m2:
                str_cap = m2.group(1)
            m3 = re.search(r"Type of model:\s(.*)", line)
            if m3:
                fwrite.write('.lib ' + m3.group(1) + '\n\n')
            m4 = re.search(r"Voltage value, in volts:\s(.*)", line)
            if m4:
                fwrite.write('.param ' + m4.group(1) + '\n')
            m5 = re.search(r"Temperature value, in degrees:\s(.*)", line)
            if m5:
                fwrite.write('.temp ' + m5.group(1) + '\n\n')
            m6 = re.search(r"Initial value of current, in micro amps:\s(.*)", line)
            if m6:
                str_c1 = m6.group(1)
            m7 = re.search(r"Pulsed value of current, in micro amps:\s(.*)", line)
            if m7:
                str_c2 = m7.group(1)
            m8 = re.search(r"Rise delay time, in nano seconds:\s(.*)", line)
            if m8:
                str_c3 = m8.group(1)
            m9 = re.search(r"Rise time constant, in pico seconds:\s(.*)", line)
            if m9:
                str_c4 = m9.group(1)
            m10 = re.search(r"Fall delay time, in nano seconds:\s(.*)", line)
            if m10:
                str_c5 = m10.group(1)
            m11 = re.search(r"Fall time constant, in pico seconds:\s(.*)", line)
            if m11:
                str_c6 = m11.group(1)
            m12 = re.search(r"Width, in micro meters:\s(.*)", line)
            if m12:
                str_t1 = m12.group(1)
            m13 = re.search(r"Length, in micro meters:\s(.*)", line)
            if m13:
                str_t2 = m13.group(1)
            m14 = re.search(r"Area source, pico:\s(.*)", line)
            if m14:
                str_t3 = m14.group(1)
            m15 = re.search(r"Area drain, pico:\s(.*)", line)
            if m15:
                str_t4 = m15.group(1)
            m16 = re.search(r"Perimeter source, in micro:\s(.*)", line)
            if m16:
                str_t5 = m16.group(1)
            m17 = re.search(r"Perimeter drain, in micro:\s(.*)", line)
            if m17:
                str_t6 = m17.group(1)
            m18 = re.search(r"Model NMOS:\s(.*)", line)
            if m18:
                str_nmodel = m18.group(1)
            m19 = re.search(r"Model PMOS:\s(.*)", line)
            if m19:
                str_pmodel = m19.group(1)
            m20 = re.search(r"Model:\s(.*)", line)
            if m20:
                str_model = m20.group(1)

    fwrite.write('.option post probe measform=2\n\n')
    fwrite.write(
        '.param i0=' + str_c1 + 'U ' + 'ia=' + str_c2 + 'U ' + 'trd=' + str_c3 + 'N ' + 'tr=' + str_c4 + 'P ' + 'tdf=' + str_c5 + 'N ' + 'tf=' + str_c6 + 'P \n')
    fwrite.write(
        '.param w=' + str_t1 + 'U ' + 'l=' + str_t2 + 'U ' + 'as=' + str_t3 + 'P ' + 'ad=' + str_t4 + 'P ' + 'ps=' + str_t5 + 'U ' + 'pd=' + str_t6 + 'U \n\n')
    fwrite.write(
        '.SUBCKT EXP_N D G S B ia1=ia trd1=trd tr1=tr tdf1=tdf tf1=tf w1=w l1=l as1=as ad1=ad ps1=ps pd1=pd \n')
    fwrite.write('I1 D B exp(i0 ia1 trd1 tr1 tdf1 tf1) \n')
    fwrite.write('MMn D G S B ' + str_nmodel + ' w=w1 l=l1 as=as1 ad=ad1 ps=ps1 pd=pd1 \n')
    fwrite.write('.ENDS \n\n')
    fwrite.write(
        '.SUBCKT EXP_P D G S B ia1=ia trd1=trd tr1=tr tdf1=tdf tf1=tf w1=w l1=l as1=as ad1=ad ps1=ps pd1=pd \n')
    fwrite.write('I2 B D exp(i0 ia1 trd1 tr1 tdf1 tf1) \n')
    fwrite.write('MMp D G S B ' + str_pmodel + ' w=w1 l=l1 as=as1 ad=ad1 ps=ps1 pd=pd1 \n')
    fwrite.write('.ENDS \n\n')

    Parameters_of_transistors = parameters_of_transistors_parser(hspice_file)
    Truth_table = truth_table(len(circuit.inputs))

    global ITER
    for i in Truth_table:
        inp_signal = dict(zip(circuit.inputs, i))
        modes, out_signals = circuit.simulate(inp_signal)

        for tr, cur in modes.items():

            if cur == 0:
                continue
            else:
                cur = str_c2
            ITER += 1
            test = {}

            for tr_ in modes:
                test[tr_] = 0
            test[tr] = cur
            fwrite.write('.alter ' + str(i) + ' ' + tr + '\n')
            fwrite.write('.SUBCKT ' + circuit.name + ' ')
            for k in circuit.inputs:
                fwrite.write(k + ' ')
            for k in circuit.outputs:
                fwrite.write(k + ' ')
            fwrite.write('VDD VSS \n')
            for tr_name, param in circuit.transistors.items():

                if param.model == 'NMOS_VTL':
                    fwrite.write('xu_' + param.label + ' ' + param.pins['drain'].label + ' ' + param.pins['gate'].label + ' ' + param.pins['source'].label + ' ' + param.pins['bulk'].label + ' EXP_N ' + 'ia1=' + str(test[param.label]) + 'U trd1=\'trd\' tr1=\'tr\' tdf1=\'tdf\' tf1=\'tf\' ' + Parameters_of_transistors[param.label] + '\n')
                else:
                    fwrite.write('xu_' + param.label + ' ' + param.pins['drain'].label + ' ' + param.pins['gate'].label + ' ' + param.pins['source'].label + ' ' + param.pins['bulk'].label + ' EXP_P ' + 'ia1=' + str(test[param.label]) + 'U trd1=\'trd\' tr1=\'tr\' tdf1=\'tdf\' tf1=\'tf\' ' + Parameters_of_transistors[param.label] + '\n')

            fwrite.write('.ENDS \n\n')
            fwrite.write('xu ')
            for k in circuit.inputs:
                fwrite.write(k + ' ')
            for k in circuit.outputs:
                fwrite.write(k + ' ')
            fwrite.write('VDD VSS ')
            fwrite.write(circuit.name + ' \n')

            fwrite.write('V' + 'VSS' + ' ' + 'VSS' + ' GND ' + 'dc=0' + '\n')
            fwrite.write('V' + 'VDD' + ' ' + 'VDD' + ' GND ' + 'dc=\'vdd\'' + '\n')
            for j in range(len(circuit.inputs)):
                if i[j] == 1:
                    fwrite.write('V' + circuit.inputs[j] + ' ' + circuit.inputs[j] + ' GND ' + 'dc=\'VDD\'' + '\n')
                else:
                    fwrite.write('V' + circuit.inputs[j] + ' ' + circuit.inputs[j] + ' GND ' + 'dc=0' + '\n')
            fwrite.write('.tran \'0.5p\' \'5n\'' + '\n')
            fwrite.write('.probe v(*)i(*)' + '\n\n')

            for k in circuit.outputs:

                if out_signals[k] == 'H':

                    fwrite.write('.meas tran Vmin' + k + ' min v(' + k + ') \n')
                    fwrite.write('.meas stuck_at_zero_' + k + ' param = ' + '\'vdd-Vmin' + k + '\'' + '\n')

                else:
                    fwrite.write('.meas tran stuck_at_one_' + k + ' max v(' + k + ') \n')
                fwrite.write('cload_' + k + ' ' + k + ' gnd ' + str_cap + 'f\n')
            fwrite.write('\n')

    fwrite.write('.end \n')

    fwrite.close()

def file_read_mt(file1, hspice_file, vdd):
    global ERRORS_ST_ONE
    global ERRORS_ST_ZERO
    fwrite = open('proverka', 'a')
    number = 0
    input_signal_list = list()
    input_signal_int = list()
    with open(file1, 'r') as f:
        for line in f:
            input_signal = re.search(r"^\.TITLE\s\'\[(.*)\]\sm_i_\d+\'\n", line) 
            
            if input_signal:                
                input_signal_list = input_signal.group(1).split()           
                
        for i in range(len(input_signal_list)):
            if len(input_signal_list) > 1:
                input_signal_int.append(int(input_signal_list[i].replace(',','')))
            else:
                input_signal_int.append(int(input_signal_list[i]))  
        
        circuit = Circuit.from_sp(hspice_file)
        inp_signal = dict(zip(circuit.inputs, input_signal_int))
        fwrite.write(str(input_signal_int) + '=')
        modes, out_signals = circuit.simulate(inp_signal) 
        
        for name, value in out_signals.items():
            with open(file1, 'r') as f:
                for line in f:
                    string0 = re.search(r"^stuck_at_one_(.*)\s=\s+([0-9]*[.,]?[0-9]*e?-?\d+?)         from =   0.         to = 5.000e-009\n" , line)
                    string1 = re.search(r"^stuck_at_zero_(.*)\s=\s+([0-9]*[.,]?[0-9]*e?-?\d+?)         from =   0.         to = 5.000e-009\n", line)
                    string00 = re.search(r"^stuck_at_one_(.*)\s=\s+([0-9]*[.,]?[0-9]*e?-?\d+?)\n" , line)
                    string11 = re.search(r"^stuck_at_zero_(.*)\s=\s+([0-9]*[.,]?[0-9]*e?-?\d+?)\n", line)
                    if string0:
                                             
                        if (string0.group(1) == name.lower() and value == 'L'):
                            
                            if float(string0.group(2)) >= float(vdd)/2:
                                if number == 0:
                                    fwrite.write('error\n')
                                    ERRORS_ST_ONE += 1
                                    number += 1
                               
                    elif string1: 
                        
                                                 
                        if (string1.group(1).lower() == name.lower() and value =='H'):   
                            
                            if float(string1.group(2)) >= float(vdd)/2:
                                if number == 0:
                                    fwrite.write('error\n')
                                    ERRORS_ST_ZERO += 1  
                                    number += 1  
                    elif string00:                    
                        if (string00.group(1) == name.lower() and value == 'L'):
                            
                            if float(string00.group(2)) >= float(vdd)/2:
                                
                                if number == 0:
                                    fwrite.write('error\n')
                                    ERRORS_ST_ONE += 1
                                    number += 1  
                    elif string11: 
                                               
                        if (string11.group(1).lower() == name.lower() and value =='H'):   

                            if float(string11.group(2)) >= float(vdd)/2:
                                if number == 0:
                                    fwrite.write('error\n')
                                    ERRORS_ST_ZERO += 1  
                                    number += 1                            
        fwrite.write('\n')    

def main_programm(folder, param_file):
    global ITER
    global ERRORS_ST_ONE
    global ERRORS_ST_ZERO
    fwrite = open('time', 'w')

    with open(param_file, 'r') as fread:
        for line in fread:
            m1 = re.search(r"Pulsed value of current, in micro amps:\s(.*)", line)
            if m1:
                str_c2 = m1.group(1)
            m2 = re.search(r"If you want to use multithreading enter thread_count:\s(.*)", line)
            if m2:
                mt = m2.group(1)

            m3 = re.search(r"Voltage value, in volts: vdd = (.*)", line)
            if m3:
                vdd = m3.group(1)

    circuits = os.listdir(os.path.join(os.path.dirname(__file__), folder))
    results_output = {}
    dict_stuck_at = {}
    for cname in circuits:
        ITER = 0

        ERRORS_ST_ONE = 0
        ERRORS_ST_ZERO = 0
        

        hspice_parser(os.path.join(folder, cname), param_file)
        t0 = time.time()
        try:
            if mt == '0':
                subprocess.call(['hspice64', 'new_hspice_file.sp'], cwd=os.path.abspath(os.curdir))
            else:
                subprocess.call(['hspice64', '-mt ', mt, ' -i ', 'new_hspice_file.sp'], cwd=os.path.abspath(os.curdir))
        except FileNotFoundError:
            pass
            print('You need SPICE program provided by Synopsys company (HSPICE)')
            break
        t1 = time.time()
        if not os.path.isdir(cname + '_' + str_c2):
            os.mkdir(cname + '_' + str_c2)

        files = os.listdir(os.curdir)
        results = filter(lambda x: x.startswith('new_hspice_file'), files)

        for i in results:
            if os.path.isfile(i):
                if i.startswith('new_hspice_file.mt'):
                    file_read_mt(i, os.path.join(folder, cname),float(vdd))
                    shutil.move(i, cname + '_' + str_c2+ '/' + i)
                elif i.startswith('new_hspice_file.sp') or i.startswith('new_hspice_file.tr'):
                    shutil.move(i, cname + '_' + str_c2 + '/' + i)
                else:
                    os.remove(i)


        line_result = []
        circuit = Circuit.from_sp(os.path.join(folder, cname))
        Truth_table = truth_table(len(circuit.inputs))
        for i in Truth_table:
            sim = 0
            error = 0
            with open('proverka', 'r') as fread:
                for line in fread:   
                    line = line[:-1].strip()     
                    if line.startswith(str(i)):
                        pair = line.split('=')        
                        param, val = pair
                        param = param.strip()
                        val = val.strip()
                        if val == 'error':
                            error = error + 1
                            sim = sim + 1
                        elif val == '':
                            sim = sim + 1
                result = float(error)/float(2*sim)
                line_result.append(result) 

        ERRORS = ERRORS_ST_ONE + ERRORS_ST_ZERO
        fwrite.write(cname + '\n')
        fwrite.write('ERRORS_ST_ONE =' + str(ERRORS_ST_ONE) + '\n')
        fwrite.write('ERRORS_ST_ZERO =' + str(ERRORS_ST_ZERO) + '\n')
        fwrite.write('ERRORS =' + str(ERRORS) + '\n')
        fwrite.write('ITER =' + str(ITER) + '\n')

        fwrite.write('time' + ': ' + str(t1 - t0) + ' s\n')
        results_output[cname] = (ERRORS, 2*ITER)
        dict_stuck_at[cname] = line_result
        os.remove('proverka')
    fwrite.close()
    with open('dict_stuck_at', 'w') as f:
        f.write(str(dict_stuck_at))
    f.close()
    with open('error_rates.txt', 'w') as f:
        for cname, (err, it) in results_output.items():
            f.write(cname + ': ' + str(float(err)/float(it)) + '\n')
    f.close()
main_programm(sys.argv[1], sys.argv[2])
