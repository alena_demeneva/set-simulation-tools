#!/usr/bin/python

import re
import os
import sys
from shutil import rmtree
import shutil

def percer_verilog_to_hspice_transistor_level(folder_spice,folder_verilog):
    if os.path.isdir('/home/intern_icdm/Desktop/demeneva/Demeneva_gitlab_15.10.2019/spice_file_shemes'):
        rmtree('/home/intern_icdm/Desktop/demeneva/Demeneva_gitlab_15.10.2019/spice_file_shemes')
    os.mkdir('/home/intern_icdm/Desktop/demeneva/Demeneva_gitlab_15.10.2019/spice_file_shemes')
    schemes = os.listdir(os.path.join(os.path.dirname(__file__),folder_verilog))
    for shemename in schemes:
        with open(os.path.join(folder_verilog, shemename), 'r') as fverilog:
            lines = fverilog.readlines() 
            fwrite = open(shemename[:-3] + 'sp', 'w')
            fwrite.write('*' * 70 + '\n')
            fwrite.write('.GLOBAL VDD \n')
            fwrite.write('.GLOBAL VSS \n\n')
            fwrite.write('*' * 70 + '\n')
            fwrite.write('* \n')
            fwrite.write('* Shemename:  '  + shemename[:-3] + '\n')
            fwrite.write('* \n')
            fwrite.write('* Technology: NCSU FreePDK 45nm.\n')
            fwrite.write('* Format:     Default. \n')
            inputs = lines[0][:-2].split()
            outputs = lines[1][:-2].split()
            fwrite.write('* Inputs:     '  + ' '.join(inputs[1:]) + '\n')
            fwrite.write('* Outputs:    '  + ' '.join(outputs[1:]) + '\n')    
            fwrite.write('*' * 70 + '\n')
            fwrite.write('.SUBCKT ' + shemename[0:-4] + ' ' + ' '.join(inputs[1:]) + ' ' + ' '.join(outputs[1:]) + ' VDD VSS \n')
            cells = os.listdir(os.path.join(os.path.dirname(__file__),folder_spice))
            pattern1 = re.compile(r'\w+')
            iter = 0
            for i in lines:
                for cellname in cells:     
                    if pattern1.findall(i)[0] == cellname:
                        iter +=1
                        with open(os.path.join(folder_spice, cellname), 'r') as fcell:
                            for line in fcell:
                                if line.startswith('M_i') == True:
                                    splitted_line = line.split()
                                    list_line = list()
                                    for j in range(len(splitted_line)):
                                        if splitted_line[j] == 'ZN' or splitted_line[j] == 'Z':
                                            splitted_line[j] = pattern1.findall(i)[-1]
                                        elif splitted_line[j] == 'A2' or splitted_line[j] == 'A':
                                            splitted_line[j] = pattern1.findall(i)[-2]  
                                        elif splitted_line[j] == 'A1':
                                            splitted_line[j] = pattern1.findall(i)[-3]    
                                        elif j<5 and splitted_line[j] != 'VSS' and splitted_line[j] != 'VDD':
                                            splitted_line[j] = str(splitted_line[j]) + '_' + str(iter)        
                                        list_line.append(splitted_line[j])
                                    fwrite.write(' '.join(list_line) + '\n')   
            fwrite.write('.ENDS \n\n') 
            fwrite.write('*' * 70 + '\n')                         
            fwrite.write('* \n')      
            fwrite.write('* END \n')
            fwrite.write('* \n')
            fwrite.write('*' * 70 + '\n')    


        fwrite.close()
        str_path = '/home/intern_icdm/Desktop/demeneva/Demeneva_gitlab_15.10.2019/' + shemename[:-3] + 'sp'
        str_path_new = '/home/intern_icdm/Desktop/demeneva/Demeneva_gitlab_15.10.2019/spice_file_shemes/' + shemename[:-3] + 'sp'
        shutil.move(str_path, str_path_new)

percer_verilog_to_hspice_transistor_level(sys.argv[1],sys.argv[2])
