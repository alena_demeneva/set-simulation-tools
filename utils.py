def full_tt(ninp):
    tt = [[]]
    for i in range(ninp):
        tt0 = [[0] + row for row in tt]
        tt1 = [[1] + row for row in tt]
        tt = tt0 + tt1
    return tt


def iter_tt(inputs):
    inputs = list(inputs)
    for vals in full_tt(len(inputs)):
        yield dict(zip(inputs, vals))


def dump_exh_inputs(inputs, file):
    inputs = sorted(inputs)
    with open(file, 'w') as f:
        f.write(' '.join(inputs) + '\n')
        for row in full_tt(len(inputs)):
            row = map(str, row)
            f.write(''.join(row) + '\n')
