from networkx import Graph
from functools import reduce
from utils import iter_tt
from time import time


_pmos_table = {('H', 'H'): 'Z',
               ('H', 'Z'): 'Z',
               ('L', 'H'): 'H',
               ('L', 'Z'): 'Z',
               ('X', 'H'): 'H',
               ('X', 'Z'): 'Z'}


_nmos_table = {('H', 'L'): 'L',
               ('H', 'Z'): 'Z',
               ('L', 'L'): 'Z',
               ('L', 'Z'): 'Z',
               ('X', 'L'): 'L',
               ('X', 'Z'): 'Z'}


class Transistor:
    def __init__(self, label: str, drain: 'Net', gate: 'Net', source: 'Net', bulk: 'Net', model: str):
        self.label = label
        self.pins = {}
        drain.connect(self, 'drain')
        gate.connect(self, 'gate')
        source.connect(self, 'source')
        bulk.connect(self, 'bulk')
        self.model = model

    def propagate(self, gate_signal, source_signal):
        if self.model == 'PMOS_VTL':
            return _pmos_table[(gate_signal, source_signal)]
        return _nmos_table[(gate_signal, source_signal)]


class Net:
    def __init__(self, label):
        self.label = label
        self.connections = set()

    def connect(self, transistor: Transistor, pin: str):
        self.connections.add((transistor, pin))
        transistor.pins[pin] = self

    def disconnect(self, transistor: Transistor, pin: str):
        self.connections.remove((transistor, pin))
        transistor.pins[pin] = None


class Circuit:
    def __init__(self, name: str = None):
        self.name = name
        self.inputs = []
        self.outputs = []
        self.transistors = {}
        self.nets = {}

    @classmethod
    def from_sp(cls, file: str):
        circuit = cls()

        with open(file, 'r') as f:
            for line in f:
                line = line[:-1].strip()

                if line.startswith('*'):
                    pair = line.strip('*').split(':')
                    if len(pair) != 2:
                        continue
                    param, val = pair
                    param = param.strip()
                    val = val.strip()
                    if param.lower() == 'inputs':
                        circuit.inputs.extend(val.split())
                    elif param.lower() == 'outputs':
                        circuit.outputs.extend(val.split())

                elif line.startswith('.SUBCKT'):
                    _, circuit.name, _ = line.split(maxsplit=2)

                elif line.startswith('M'):
                    label, d, g, s, b, model, _ = line.split(maxsplit=6)
                    for net_label in (d, g, s, b):
                        if net_label not in circuit.nets:
                            circuit.nets[net_label] = Net(net_label)
                    circuit.transistors[label] = Transistor(label, *map(circuit.nets.get, (d, g, s, b)), model)

        return circuit

    def simulate(self, inputs: dict, errors: list, get_closed_transistors: bool = False):
        """
        :param inputs: {<net name>: <signal value>, ...}
        :param errors: [<tr. name>, ...]
        :param get_closed_transistors: if True return list of closed transistors additionally (doesn't work with Xs)
        :return: {<net name>: <signal value>, ...}, optional [<tr name>, ...]
        """
        signals = {self.nets[net_name]: 'H' if val else 'L' for net_name, val in inputs.items()}
        signals[self.nets['VDD']] = 'H'
        signals[self.nets['VSS']] = 'L'
        temp_signals = {net: set() for net in self.nets.values() if net not in signals}

        net_unknown_inputs = {}
        tr_unknown_inputs = {}

        for net in self.nets.values():
            if net in signals:
                continue
            inps = 0
            for tr, pin in net.connections:
                if pin == 'drain':
                    inps += 1
            net_unknown_inputs[net] = inps

        for tr in self.transistors.values():
            inps = 0
            if tr.pins['gate'] not in signals:
                inps += 1
            if tr.pins['source'] not in signals:
                inps += 1
            tr_unknown_inputs[tr] = inps

        while tr_unknown_inputs:
            tr_ready = [tr for tr, inps in tr_unknown_inputs.items() if inps == 0]
            for tr in tr_ready:
                del tr_unknown_inputs[tr]

                gate_net = tr.pins['gate']
                gate_signal = signals[gate_net]
                source_net = tr.pins['source']
                source_signal = signals[source_net]

                drain_signal = source_signal if tr.label in errors else tr.propagate(gate_signal, source_signal)
                drain_net = tr.pins['drain']
                temp_signals[drain_net].add(drain_signal)
                net_unknown_inputs[drain_net] -= 1

            net_ready = [net for net, inps in net_unknown_inputs.items() if inps == 0]
            for net in net_ready:
                del net_unknown_inputs[net]

                temp = temp_signals[net]
                if 'H' in temp and 'L' in temp:
                    signals[net] = 'X'
                elif 'H' in temp:
                    signals[net] = 'H'
                elif 'L' in temp:
                    signals[net] = 'L'
                else:
                    signals[net] = 'Z'

                for tr, pin in net.connections:
                    if pin in ['source', 'gate']:
                        tr_unknown_inputs[tr] -= 1

        rename = {'L': '0',
                  'H': '1',
                  'X': 'X',
                  'Z': 'Z'}

        if not get_closed_transistors:
            return {out_name: rename[signals[self.nets[out_name]]] for out_name in self.outputs}

        closed_trs = []
        for tr in self.transistors.values():
            gate_signal = signals[tr.pins['gate']]
            if (gate_signal == 'L') != (tr.model == 'PMOS_VTL'):
                closed_trs.append(tr.label)

        return {out_name: rename[signals[self.nets[out_name]]] for out_name in self.outputs}, closed_trs

    def pof(self, timed=True):
        t0 = time()
        nerr = 0
        nsim = 0
        for input_vector in iter_tt(self.inputs):
            input_vector = {inp: 'H' if val else 'L' for inp, val in input_vector.items()}
            ref_output, closed_trs = self.simulate(input_vector, [], get_closed_transistors=True)
            nsim += len(closed_trs)
            for tr in closed_trs:
                err_output = self.simulate(input_vector, [tr])
                if ref_output != err_output:
                    nerr += 1

        if timed:
            return nerr / nsim, time() - t0
        else:
            return nerr / nsim


class SPModel(Graph):
    @classmethod
    def from_circuit(cls, circuit):
        model = cls()

        model.circuit = circuit
        model.transmit_nodes = set()
        model.inputs = set(circuit.inputs)
        model.outputs = set(circuit.outputs)
        for net in circuit.nets.values():
            tr_models = set()
            for tr, pin in net.connections:
                if pin in ['source', 'drain']:
                    tr_models.add(tr.model)
            if len(tr_models) == 2:
                model.transmit_nodes.add(net.label)

        for net in circuit.nets:
            if net not in model.inputs:
                model.add_node(net)

        for tr in circuit.transistors.values():
            switch = Switch(tr.label, tr.pins['gate'].label, (tr.model == 'NMOS_VTL') + 1)
            model.add_edge(tr.pins['source'].label, tr.pins['drain'].label, switch=switch)

        return model

    def add_edge(self, u_of_edge, v_of_edge, **attr):
        if not self.has_edge(u_of_edge, v_of_edge):
            super().add_edge(u_of_edge, v_of_edge, inst=[])
        self.edges[u_of_edge, v_of_edge]['inst'].append(attr)

    def _reduce_edge(self, u, v):
        return Parallel(*(inst['switch'] for inst in self.edges[u, v]['inst']))

    def reduce(self):
        removed = True
        while removed:
            removed = []
            for node in self.nodes:
                if node in ['VDD', 'VSS'] or node in self.transmit_nodes:
                    continue
                nei = list(self.neighbors(node))
                if len(nei) != 2:
                    continue
                u, v = nei
                switch = Series(self._reduce_edge(u, node), self._reduce_edge(node, v))
                removed.append(node)
                self.add_edge(u, v, switch=switch)
            for node in removed:
                self.remove_node(node)

        for node in self.transmit_nodes:
            pull_up = self._reduce_edge(node, 'VDD')
            pull_down = self._reduce_edge(node, 'VSS')
            super().add_edge(node, 'VDD', switch=pull_up)
            super().add_edge(node, 'VSS', switch=pull_down)

    def topological_sort(self):
        if hasattr(self, 'topological_order'):
            return
        nodes = [node for node in self.nodes if node not in ['VDD', 'VSS']]
        fanout = {node: set() for node in nodes}
        fanin = {node: 0 for node in nodes}

        for node in nodes:
            pu = self.edges[node, 'VDD']['switch']
            pd = self.edges[node, 'VSS']['switch']
            fi = set.union(pu.controls(), pd.controls()).difference(self.inputs)
            fanin[node] = len(fi)
            for pred in fi:
                fanout[pred].add(node)

        top_order = []

        while True:
            ready = [node for node, inps in fanin.items() if inps == 0]
            if len(ready) == 0:
                break
            top_order.extend(ready)
            for node in ready:
                for succ in fanout[node]:
                    fanin[succ] -= 1
            for node in ready:
                del fanin[node]

        self.topological_order = top_order

    def simulate(self, inputs, stuck):
        self.topological_sort()
        signals = {inp: val + 1 for inp, val in inputs.items()}
        reverse_conversion = {0b01: '0',
                              0b10: '1',
                              0b11: 'X'}

        for node in self.topological_order:
            pu = self.edges[node, 'VDD']['switch']
            pd = self.edges[node, 'VSS']['switch']
            if pu(signals, stuck):
                signals[node] = 0b11 if pd(signals, stuck) else 0b10
            elif pd(signals, stuck):
                signals[node] = 0b01
            else:
                raise Exception('Value computation at node {} failed: both pull-up and pull-down paths are closed'.format(node))

        return {out: reverse_conversion[signals[out]] for out in self.outputs}

    def pof(self, timed=True):
        t0 = time()
        nerr = 0

        for input_vector in iter_tt(self.inputs):
            ref_output = self.simulate(input_vector, [])
            for tr in self.circuit.transistors:
                err_output = self.simulate(input_vector, [tr])
                if ref_output != err_output:
                    nerr += 1

        if timed:
            return nerr / 2 ** len(self.inputs) / len(self.circuit.transistors), time() - t0
        else:
            return nerr / 2 ** len(self.inputs) / len(self.circuit.transistors)


class Switch:
    def __init__(self, name, control, val):
        self.name = name
        self.control = control
        self.val = val

    def __call__(self, inputs, stuck):
        if self.name in stuck:
            return True
        if self.control in inputs:
            return inputs[self.control] & self.val
        return False

    def controls(self):
        return {self.control}


class Array:
    def __new__(cls, *switch):
        if len(switch) == 1:
            return switch[0]
        obj = object.__new__(cls)
        obj.__init__(*switch)
        return obj

    def __init__(self, *switch):
        switches = []
        for sw in switch:
            if isinstance(sw, self.__class__):
                switches.extend(sw.switches)
            else:
                switches.append(sw)
        self.switches = switch

    def controls(self):
        return reduce(set.union, (sw.controls() for sw in self.switches), set())


class Series(Array):
    def __call__(self, inputs, stuck):
        if all(isinstance(sw, Switch) for sw in self.switches) and any(sw.name in stuck for sw in self.switches):
            return True
        return all(switch(inputs, stuck) for switch in self.switches)


class Parallel(Array):
    def __call__(self, inputs, stuck):
        return any(switch(inputs, stuck) for switch in self.switches)


if __name__ == '__main__':
    from argparse import ArgumentParser
    import sys
    import os

    parser = ArgumentParser()
    parser.add_argument('input_directory', help='Path to directory with input .sp files')
    parser.add_argument('output_file', help='Path to output log file')
    args = parser.parse_args(sys.argv[1:])

    input_dir = args.input_directory
    output_file = args.output_file

    clist = os.listdir(input_dir)
    res = []

    for cname in clist:
        print(cname)
        cpath = os.path.join(input_dir, cname)
        circuit = Circuit.from_sp(cpath)
        sp = SPModel.from_circuit(circuit)
        sp.reduce()
        sp.topological_sort()
        res.append([cname.split('.', maxsplit=1)[0], *sp.pof()])

    with open(output_file, 'w') as f:
        for c, p, t in res:
            f.write('Circuit: {}\n'
                    'PoF: {:.2f}%\n'
                    'Time: {:.2f}ms\n\n'.format(c, p * 100, t * 1000))